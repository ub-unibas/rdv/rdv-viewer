import {environment as loc} from "@env_temp/environment.type-loc";

const Proj = "swa_pdfs";
import {initRdvLib, SettingsModel} from 'rdv-lib';
import {environment as proj} from '@env/swa_pdfs/environment';
import {environment as prod} from '@env_temp/environment.type-prod';

export const environment: SettingsModel = {
  ...proj,
  ...prod,
  documentViewerProxyUrl: undefined,

  proxyUrl : prod.proxyUrl +  Proj + "/",
  moreProxyUrl: prod.moreProxyUrl +  Proj + "/",
  inFacetSearchProxyUrl: prod.inFacetSearchProxyUrl +  Proj + "/",
  detailProxyUrl: prod.detailProxyUrl +  Proj + "/",
  navDetailProxyUrl: prod.navDetailProxyUrl +  Proj + "/",
  popupQueryProxyUrl: prod.popupQueryProxyUrl +  Proj + "/",
  detailSuggestionProxyUrl: prod.detailSuggestionProxyUrl +  Proj + "/",
  suggestSearchWordProxyUrl: prod.suggestSearchWordProxyUrl +  Proj + "/",
  detailEditProxyUrl: "https://ub-rdv-proxy.ub.unibas.ch/v1/rdv_object/object_edit/" +  Proj + "/",
  detailNewProxyUrl: "https://ub-rdv-proxy.ub.unibas.ch/v1/rdv_object/object_new/" +  Proj + "/",
  creatable: [
    {
      label: {
        "de": "neues PDF",
        "en": "scanned press clipping"
      },
      value: "swadok_pdfs"
    }
  ],
};
initRdvLib(environment);
