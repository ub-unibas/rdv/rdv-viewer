/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {

  // Defaultsprache für fehlende Übersetzungen in anderen Sprachen
  defaultLanguage: 'de',

  // optional pro Sprache environment-spezifische Übersetzungen
  // i18n: {
  //   "de": {
  //     "beta-header.contact": "XXX Kontaktieren Sie bei Fragen: ",
  //   },
  //   "en": {
  //     "beta-header.contact": "YYY Contact: ",
  //   }
  // },

  production: false,

  showSimpleSearch: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-itb.ub.unibas.ch/",
  narrowEmbeddedIiiFViewer: 'bottom',
  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
  // Auswahlmöglichkeit gegeben
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 25}, {view: Page.Detail, order: 25}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "obj_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Illustration", value: {"id": "Illustration"}}
          ]
        }
      ],
    },
    'UVScroll': {
      enabled: true,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "obj_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Illustration", value: {"id": "Illustration"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: true,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",
      disabledPlugins: ["text-overlay"],
      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",
      // Optional kann pro Breakpoint eine andere Konfiguration geladen werden.
      // Prinzipiell werden die Konfigurationen verwendet, wo die aktuelle Browserbreite unterhalb des maxWidth-Wertes liegt.
      // Von allen in Frage kommenden Definitionen wird diejenige mit der kleinsten Differenz von
      // "maxWidth - <aktuelle Breite>" verwendet.
      // Anm.: Die Reihenfolge der Definitionen ist egal.
      //viewerConfigMobilUrl: [
      // {maxWidth: 700, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"},
      //{maxWidth: 500, url: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_mobile.json"}
      //],
      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "obj_type",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Illustration", value: {"id": "Illustration"}}
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,
  editable: false,
  detailEditProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_edit/",

  externalAboutUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",
  externalHelpUrl: "https://ub.unibas.ch/de/historische-bestaende/wirtschaftsdokumentation/zas-portal-infos/",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "sort_obj",
      order: SortOrder.ASC,
      display: "select-sort-fields.score_obj"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "select-sort-fields.date_asc"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "select-sort-fields.date_desc"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "obj_type": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Objekt Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "initiallyOpen": true
    },
    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Autor",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 20,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
    },
    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 21,
      "expandAmount": 700,
      "size": 700,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL
    },
    "rel_persons": {
      "field": "rel_persons.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag (700er)",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 25,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true,
    },
    "rel_persons2": {
      "field": "rel_persons2",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Rolle",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 26,
      "expandAmount": 500,
      "size": 500,
    },
    "rel_persons2 Auswahl": {
      "field": "rel_persons2.Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Auswahl",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 27,
      "expandAmount": 500,
      "size": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
    },
    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Druckjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 28,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 29,
      "size": 100,
      "expandAmount": 5,
    },


    "Erstdrucke": {
      "field": "Erstdrucke.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Erst- und Teilerstdrucke",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 30,
      "size": 100,
      "expandAmount": 5,
    },
    "Widmungsexemplare": {
      "field": "Widmungsexemplare.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Widmungsexemplare",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 31,
      "size": 100,
      "expandAmount": 5,
    },
    "Adressaten": {
      "field": "Adressaten.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Adressaten von Widmungen",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 32,
      "size": 500,
      "expandAmount": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL],
    },
    "hierarchy_filter": {
      "field": "hierarchy_filter.keyword",
      "facetType": FacetFieldType.HIERARCHIC,
      "label": "Griechischer Geist Inhaltsverzeichnis",
      "operator": "AND",
      "order": 10,
      "size": 100,
      "expandAmount": 100
    },
    "GG-ID": {
      "field": "gg_id.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Griechischer Geist Nummer",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 11,
      "size": 500,
      "expandAmount": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL
    },
    "GG-Rel-ID": {
      "field": "related_gg.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Griechischer Geist Referenz",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 12,
      "size": 500,
      "expandAmount": 500,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL
    },

    "GG-Seiten": {
      "field": "img_type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Griechischer Geist Illustration",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 13,
      "size": 100,
      "expandAmount": 10,
    },

    "themen-hist": {
      "field": "themen-hist.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen: historisch",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL
    },
    "themen-lit": {
      "field": "themen-lit.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen: literaturhistorisch",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL
    },
    "themen-myth": {
      "field": "themen-myth.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen: mythologisch",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.LABEL, FacetValueOrder.COUNT]
    },
    "themen-phil": {
      "field": "themen-phil.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen: philos./theol.",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL
    },
    "themen-poet": {
      "field": "themen-poet.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Themen: poetisch",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "size": 100,
      "expandAmount": 5,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL
    },
  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 50,
    "offset": 0,
    "sortField": "sort_obj",
    "sortOrder": SortOrder.ASC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Griechischer Geist aus Basler Pressen",
      "top.headerSettings.name.Dev": "Griechischer Geist aus Basler Pressen (Dev)",
      "top.headerSettings.name.Loc": "Griechischer Geist aus Basler Pressen (Loc)",
      "top.headerSettings.name.Test": "Griechischer Geist aus Basler Pressen (Test)",
      "top.headerSettings.betaBarContact.name": "Christoph Schneider",
      "top.headerSettings.betaBarContact.email": "christoph.schneider@unibas.ch",
      "search-results.view-mode.UVScroll.object_type.message.title": "Hinweis",
      "search-results.view-mode.UVScroll.object_type.message.text": "In dieser Ansicht können nur die Illustrationen angzeigt werden.",
      "select-sort-fields.score_obj": "Objekttyp"
    }
  },

};
