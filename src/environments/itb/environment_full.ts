/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {
  Backend,
  ExtraInfoFieldType,
  FacetFieldType,
  FacetValueOrder,
  HistogramFieldModel,
  HistogramFieldType,
  Page,
  SettingsModel,
  SortOrder,
  ViewerType
} from "@app/shared/models/settings.model";

export const environment: SettingsModel = {

  // Defaultsprache für fehlende Übersetzungen in anderen Sprachen
  defaultLanguage: 'de',

  // optional pro Sprache environment-spezifische Übersetzungen
  // i18n: {
  //   "de": {
  //     "beta-header.contact": "XXX Kontaktieren Sie bei Fragen: ",
  //   },
  //   "en": {
  //     "beta-header.contact": "YYY Contact: ",
  //   }
  // },

  production: false,

  showSimpleSearch: true,
  // per Default werden Preview Images angezeigt
  // showImagePreview: false,

  backend: Backend.ELASTIC,
  // unter welcher Domaine (und Verzeichnis) läuft der RDV (wird benutzt um Links zu generieren)
  baseUrl: "https://ub-itb.ub.unibas.ch/",
  narrowEmbeddedIiiFViewer: 'bottom',
  // Header-Anzeige-Einstellungen
  headerSettings: {
    // Fallback für alle Sprachen
    "default": {
      // i18n key für den Portal-Namen
      // name: "top.headerSettings.name",
      // wenn Angabe fehlt oder false ist, wird der Header angezeigt; bei true nicht
      disable: false,
      // Portal-Namen unterhalb des Headers anzeigen
      showPortalName: true,
      // wenn true, wird ein anderer Text als Department-Name verwendet, anstatt des Portal-Namens
      // (i18n key: top.headerSettings.logoSubTitle)
      useLogoSubTitle: true,
      // optional dieses Logo an der rechten Seite anzeigen
      // (i18n key: top.headerSettings.departmentLogoUrl, top.headerSettings.departmentUrl)
      useDepartmentLogoUrl: false,
      // den Sprachauswahl-Abschnitt in der UI ausblenden bei true, ansonsten anzeigen
      // disableLanguageBar: false,
      // Beta-Balken anzeigen, wenn true (nicht angezeigt, wenn Eintrag fehlt oder false)
      // (i18n keys: top.headerSettings.betaBarContact.name, top.headerSettings.betaBarContact.email)
      showBetaBar: true
    },
    // sprachspezifische Einstellungen (ohne Übersetzungen)
    // "de": {
    // },
    // "en": {
    // }
  },

  // footerSettings: {
  //   "default": {
  //     // den ganzen Footer ausblenden
  //     disable: false,
  //     // den "Nach Oben"-Link anzeigen
  //     displayToTop: true,
  //     // Host des Projekts im Footer anzeigen ("Universität Basel")
  //     displayHostUrl: true,
  //     // i18n key der URL des Host-Links (bei displayHostUrl: true)
  //     hostUrl: "footer.footerSettings.host.url",
  //     // i18n key des Namens des Host-Links (bei displayHostUrl: true)
  //     hostName: "footer.footerSettings.host.name",
  //     // Portal-URL im Footer anzeigen
  //     displayPortalUrl: true
  //   },
  //   // sprachspezifische Einstellungen (ohne Übersetzungen)
  //   "de": {
  //   },
  //   "en": {
  //   }
  // },

  // sollten mehrere documentViewer enabled sein, wir dem User in der UI eine
  // Auswahlmöglichkeit gegeben
  documentViewer: {
    // [ViewerType.UV]: { // not working if aot/prod is built
    'UV': {
      enabled: false,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Detail, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/zas_en.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'UVScroll': {
      enabled: false,
      type: ViewerType.UV,
      enabledForPage: [{view: Page.Search, order: 20}], // default: on all views
      viewerUrl: "assets/uv/uv.html",

      iconUrl: "/assets/img/icon_UV_list.svg",

      // Extra-Konfiguration für den Universal Viewer (UV)
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-uvconfig.ub.unibas.ch/scroll_view.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Einzelseiten", value: {"id": "einzelseiten"}}
          ]
        }
      ],
    },
    // [ViewerType.MIRADOR]: { // not working if aot/prod is built
    'Mirador': {
      enabled: false,
      type: ViewerType.MIRADOR,
      enabledForPage: [{view: Page.Search, order: 10}],
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
        {
          field: "digitalisiert",
          messageTitleKey: "search-results.view-mode.UVScroll.object_type.message.title",
          messageTextKey: "search-results.view-mode.UVScroll.object_type.message.text",
          values: [
            {label: "Einzelseiten", value: {"id": "einzelseiten"}}
          ]
        }
      ],
    },
    'MiradorScroll': {
      enabled: false,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // mögliche Werte: "share", "download","image-tools","text-overlay"
      disabledPlugins: ["text-overlay"],

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_scroll.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    },
    'MiradorSinglePage': {
      enabled: true,
      enabledForPage: [{view: Page.Detail, order: 10}],
      type: ViewerType.MIRADOR,
      viewerUrl: "/assets/mirador/init.html",

      iconUrl: "/assets/img/icon_Mirador_list.svg",

      // Extra-Konfiguration für Mirador
      // ACHTUNG Wenn die CORS-Header-Einstellungen nicht zur Domain der Webapp passen, wird der UV nicht geladen!
      viewerConfigUrl: "https://ub-miradorconfig.ub.unibas.ch/ub_mirador_single.json",

      // Von der angegebenen Facette sollen alle aufgezählten Facetten-Werte bei der aktuellen Auswahl
      // erhalten bleiben. Ist kein Wert bereits ausgewählt, wird der erste Wert automatisch ausgewählt.
      // Wird ein messageTextKey: angegeben, so wird dessen lokalisierter Text als Toast Message
      // angezeigt (mit messageTitleKey: optional).
      facetRestrictions: [
      ],
    }
  },

  // UV/Mirador verwenden nur maximale Breite der app (false, undefined).
  // Wird diese Option auf true gesetzt, dann wird die volle Browser-Viewport-Breite verwendet.
  viewerExtraWide: false,

  // die konkreten Werte, werden noch gesetzt
  proxyUrl: undefined,
  moreProxyUrl: undefined,
  inFacetSearchProxyUrl: undefined,
  popupQueryProxyUrl: undefined,
  documentViewerProxyUrl: undefined,
  editable: true,
  detailEditProxyUrl: "http://127.0.0.1:5000/v1/rdv_object/object_edit/",

  externalAboutUrl: "https://ub-easyweb.ub.unibas.ch/de/ub-wirtschaft-swa/swa-446/",
  externalHelpUrl: "https://ub.unibas.ch/de/historische-bestaende/wirtschaftsdokumentation/zas-portal-infos/",

  //Welche Felder sind durchsuchbar, Anzahl der Felder in preselect regelt wie viele Suchfelder erscheinen
  searchFields: {
    "options": {
      "Quellname": "Zeitung",
      "Textdatum": "Datum-Textform",
      "all_text": "Freitext",
      "descr_fuv": "Firmen und Verb\u00e4nde",
      "descr_person": "Personen",
      "descr_sach": "Sachdeskriptor",
      "fulltext": "Volltext",
      "title": "Dossiertitel"
    },
    "preselect": [
      "all_text"
    ]
  },

  // siehe auch "queryParams" unten, dessen Wert mit dem ersten Eintrag hier übereinstimmen muss
  sortFields: [
    {
      field: "_score",
      order: SortOrder.DESC,
      display: "Relevanz"
    },
    {
      field: "sort_pos_wirkungzeit",
      order: SortOrder.ASC,
      display: "Drucker Wirkungszeit (aufsteigend)"
    },
    {
      field: "sort_pos_wirkungzeit",
      order: SortOrder.DESC,
      display: "Drucker Wirkungszeit (absteigend)"
    },
    {
      field: "year",
      order: SortOrder.ASC,
      display: "Druckjahr (aufsteigend)"
    },
    {
      field: "year",
      order: SortOrder.DESC,
      display: "Druckjahr (absteigend)"
    },
  ],

  //Infos zu Filtern (z.B. Filterung nach Einrichtung)
  filterFields: {},

  //Infos zu Facetten (z.B. mit welchen Operatoren die Facettenwere einer Facette verknuepft werden koennen)
  //order gilt fuer Facetten und Ranges
  facetFields: {
    "kategorie": {
      "field": "type.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Objekt Kategorie",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 10,
      "size": 100,
      "initiallyOpen": true
    },
    "local_code": {
      "field": "local_code.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Spezialkatalog",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 1,
      "expandAmount": 10,
      "size": 100,
    },
    "ITB Felder": {
      "field": "Felder.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "ITB Inhalte",
      "operator": "OR",
      "operators": [
        "OR", "AND"
      ],
      "order": 2,
      "expandAmount": 30,
      "size": 100,
    },
    "Bibliothek": {
      "field": "852b_4.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliothek",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 3,
      "expandAmount": 30,
      "size": 100,
    },
    "Status": {
      "field": "page_status.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Publikationsstatus",
      "operator": "OR",
      "operators": [
        "OR", "AND", "NOT"
      ],
      "order": 4,
      "expandAmount": 30,
      "size": 100,
    },

    "Drucker": {
      "field": "Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Drucker/in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 0,
      "expandAmount": 400,
      "size": 400,
      "searchWithin": true,
      "autocomplete_size": 3,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "Wirkungsbeginn": {
      "field": "wirkungsbeginn",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Drucker/in Wirkungsbeginn Basel",
      "operator": "AND",
      "showAggs": true,
      "order": 21,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "Geburtsjahr": {
      "field": "geburtsjahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Drucker/in Geburtsjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 22,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "Sterbejahr": {
      "field": "sterbejahr",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Drucker/in Sterbejahr",
      "operator": "AND",
      "showAggs": true,
      "order": 23,
      "size": 31,
      "expandAmount": 31,
    } as HistogramFieldModel,
    "Geschlecht": {
      "field": "geschlecht.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Drucker/in Geschlecht",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 24,
      "size": 100,
      "expandAmount": 10,
    },
    "Wirkungsort": {
      "field": "wirkungsort.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Drucker/in Wirkungsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 27,
      "size": 100,
      "expandAmount": 10,
      "searchWithin": true,
    },
    "Land": {
      "field": "land.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Drucker/in Herkunft",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 28,
      "size": 100,
      "expandAmount": 30,
    },


    "title": {
      "field": "title.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Titel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 40,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },

    "author": {
      "field": "author.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verfasser/in",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 41,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "year": {
      "field": "year",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.DATE,
      "label": "Druckjahr",
      "operator": "AND",
      "showAggs": true,
      "order": 42,
      "size": 31,
      "expandAmount": 31
    } as HistogramFieldModel,
    "digitalisiert": {
      "field": "digitalisiert.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Digitalisiert",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 43,
      "size": 100,
      "expandAmount": 5,
    },
    "Sprachcode": {
      "field": "041a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Sprache",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 44,
      "expandAmount": 50,
      "size": 100,
    },

    "Schlagwörter formal": {
      "field": "655a_7.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Formalschlagwort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 45,
      "expandAmount": 100,
      "size": 100,

    },
    "Hochschulschrift": {
      "field": "502a_norm.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Hochschulschrift",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 46,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "anmerkungen": {
      "field": "500a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Anmerkungen",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 47,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "Einheitstitel": {
      "field": "730a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Einheitstitel",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 48,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.COUNT,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "bib_nachweis": {
      "field": "510a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Bibliografischer Nachweis",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 49,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },

    "verlag": {
      "field": "264a_1.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Verlag",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 50,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "verlagsort": {
      "field": "751a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Druck-/Verlagsort",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 51,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },

    "rel_persons": {
      "field": "700a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "NE Person",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 52,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "rel_fuv": {
      "field": "710a.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "NE Körperschaft",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 52,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
      "valueOrder": FacetValueOrder.LABEL,
      "valueOrders": [FacetValueOrder.COUNT, FacetValueOrder.LABEL]
    },
    "rel_persons2": {
      "field": "rel_persons2",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Rolle",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 53,
      "expandAmount": 100,
      "size": 100,
    },
    "rel_persons2 Auswahl": {
      "field": "rel_persons2.Drucker.id.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Personeneintrag Auswahl",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 54,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "umfang": {
      "field": "300a.keyword",
      "facetType": FacetFieldType.SIMPLE,
      "label": "Umfang (Kollation)",
      "operator": "OR",
      "operators": [
        "OR"
      ],
      "order": 55,
      "expandAmount": 10,
      "size": 100,
      "searchWithin": true,
    },
    "umfang_norm": {
      "field": "umfang",
      "label": "Umfang (normiert) - TEST",
      "facetType": FacetFieldType.HISTOGRAM,
      "data_type": HistogramFieldType.INT,
      "operator": "AND",
      "showAggs": true,
      "order": 56,
      "size": 100,
      "expandAmount": 10
    } as HistogramFieldModel,

  },

  //Infos zu Ranges (z.B. Label)
  //order gilt fuer Facetten und Ranges
  rangeFields: {
    "Quelldatum": {
      "field": "Quelldatum.keyword",
      "facetType": FacetFieldType.RANGE,
      "label": "Quelldatum",
      "from": "1960-01-01",
      "to": "2200-01-01",
      "min": "1960-01-01",
      "max": "2200-01-01",
      "showMissingValues": true,
      "order": 1,
      "expandAmount": 5
    }
  },

  //Optionen fuer Anzahl der Treffer Treffertabelle
  rowOpts: [10, 50, 100, 200],

  // sortField/sortOrder entspricht 1.ten Listen-Element in "sortFields"
  queryParams: {
    "rows": 50,
    "offset": 0,
    "sortField": "_score",
    "sortOrder": SortOrder.DESC
  },

  //Config fuer Merkliste
  basketConfig: {
    "queryParams": {
      "rows": 10,
      "sortField": "_id",
      "sortOrder": SortOrder.ASC
    }
  },

  showExportList: {
    "basket": false,
    "table": false
  },

  //Tabellenspalten mit Sortierkriterium (Solr-Feld oder false)
  tableFields: [
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "extraInfo": true,
      "field": "title",
      "label": "Dossiertitel",
      "landingpage": true,
      "sort": "title.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quellname",
      "label": "Zeitung",
      "sort": "Quellname.keyword"
    },
    {
      "css": "col-sm-4 col-lg-4 text-left",
      "field": "Quelldatum",
      "label": "Quelldatum",
      "sort": "Quelldatum.keyword"
    }
  ],

  //Welche Felder sollen in zusaetzlicher Zeile angezeigt werden
  extraInfos: {
    "descr_fuv": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_fuv",
      "label": "Firmen und Verb\u00e4nde"
    },
    "descr_person": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_person",
      "label": "Personen"
    },
    "descr_sach": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "descr_sach",
      "label": "Sachdeskriptor"
    },
    "object_type": {
      "display": ExtraInfoFieldType.TEXT,
      "field": "object_type",
      "label": "Objekttyp"
    }
  },

  i18n: {
    "de": {
      "top.headerSettings.name": "Index typographorum editorumque Basiliensium (ITB) ",
      "top.headerSettings.name.Dev": "Index typographorum editorumque Basiliensium (ITB) (Dev)",
      "top.headerSettings.name.Loc": "Index typographorum editorumque Basiliensium (ITB) (Loc)",
      "top.headerSettings.name.Test": "Index typographorum editorumque Basiliensium (ITB) (Test)",
      "top.headerSettings.betaBarContact.name": "HAD",
      "top.headerSettings.betaBarContact.email": "hss-ub@unibas.ch",
    },
    "en": {
      "top.headerSettings.name": "Index typographorum editorumque Basiliensium (ITB) ",
      "top.headerSettings.name.Dev": "Index typographorum editorumque Basiliensium (ITB) (Dev)",
      "top.headerSettings.name.Loc": "Index typographorum editorumque Basiliensium (ITB) (Loc)",
      "top.headerSettings.name.Test": "Index typographorum editorumque Basiliensium (ITB) (Test)",
      "top.headerSettings.betaBarContact.name": "HAD",
      "top.headerSettings.betaBarContact.email": "hss-ub@unibas.ch",
    }
  },

};
