const Proj = "zas-test";

import {initRdvLib, SettingsModel} from 'rdv-lib';
import {environment as proj} from '@env/zas/environment';
import {environment as test} from '@env_temp/environment.type-test';
import {addTestNamePostfix} from "@env_temp/util";

export const environment: SettingsModel = {
  ...proj,
  ...test,
  headerSettings: addTestNamePostfix(proj.headerSettings),

  editable: true,
  proxyUrl: test.proxyUrl + Proj + "/",
  moreProxyUrl: test.moreProxyUrl + Proj + "/",
  inFacetSearchProxyUrl: test.inFacetSearchProxyUrl + Proj + "/",
  detailProxyUrl: test.detailProxyUrl + Proj + "/",
  documentViewerProxyUrl: test.documentViewerProxyUrl + Proj + "/",
  navDetailProxyUrl: test.navDetailProxyUrl + Proj + "/",
  popupQueryProxyUrl: test.popupQueryProxyUrl + Proj + "/",
  suggestSearchWordProxyUrl: undefined,
  detailEditProxyUrl: test.detailEditProxyUrl + Proj + "/"
};

initRdvLib(environment);
