/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {AfterContentInit, Directive, ElementRef, Input} from '@angular/core';

/**
 * Creates link out of input if required
 */
@Directive({
  selector: '[appDisplayLink]'
})
export class DisplayLinkDirective implements AfterContentInit {

  @Input() display: string;
  @Input() value: string;

  constructor(private el: ElementRef) {
  }

  ngAfterContentInit(): void {
    this.el.nativeElement.innerHTML = this.display === 'link' ?
      "<a href=" + this.value + ">" + this.value + " <i class='fa fa-external-link'></i></a>" :
      this.value;
  }

}
