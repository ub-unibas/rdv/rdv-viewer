/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Basket} from '../models/basket.model';
import {BasketActions, BasketActionTypes} from '../actions/basket.actions';

/**
 * List of saved baskets
 */
export interface State extends EntityState<Basket> {
  selectedBasketId: string | null;
}

/**
 * @ignore
 */
export const adapter: EntityAdapter<Basket> = createEntityAdapter<Basket>();

/**
 * @ignore
 */
export const initialState: State = adapter.getInitialState({
  selectedBasketId: null,
});

/**
 * @ignore
 */
export function reducer(
  state = initialState,
  action: BasketActions
): State {
  switch (action.type) {
    case BasketActionTypes.AddBasket: {
      return adapter.addOne(action.payload.basket, state);
    }

    case BasketActionTypes.UpsertBasket: {
      return adapter.upsertOne(action.payload.basket, state);
    }

    case BasketActionTypes.AddBaskets: {
      return adapter.addMany(action.payload.baskets, state);
    }

    case BasketActionTypes.UpdateBasket: {
      return adapter.updateOne(action.payload.basket, state);
    }

    case BasketActionTypes.DeleteBasket: {
      return adapter.removeOne(action.payload.id, state);
    }

    case BasketActionTypes.ClearBaskets: {
      return adapter.removeAll({...state, selectedBasketId: null});
    }

    case BasketActionTypes.SelectBasket: {
      return Object.assign({...state, selectedBasketId: action.payload.id})
    }

    default: {
      return state;
    }
  }
}

/**
 * @ignore
 */
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

/**
 * @ignore
 */
export const selectCurrentBasketId = (state: State) => state.selectedBasketId;
