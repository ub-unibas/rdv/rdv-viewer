/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {SimpleFacetComponent} from "./simple-facet.component";

/**
 * Displays collapsible container.
 */
@Component({
  selector: 'app-collapsible-facet',
  template: `
    <app-collapsible [title]="title" (changed)="stateChanged($event)" [key]="key" [open]="open">
      <app-facet #facet [key]="key" [visible]="open"></app-facet>
    </app-collapsible>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SimpleCollapsibleFacetComponent {
  @Input() key: string;
  @Input() title: string;
  @Input() open: boolean;
  @Output() openFacetEmitter = new EventEmitter<{ key: string, open: boolean }>();
  @ViewChild("facet") facetRef: SimpleFacetComponent;

  stateChanged(opened) {
    this.openFacetEmitter.emit({key: this.key, open: opened});
    // clear in-facet search prefix on facet close
    if (!opened) {
      this.facetRef.clearInFacetSearch();
    }
  }
}
