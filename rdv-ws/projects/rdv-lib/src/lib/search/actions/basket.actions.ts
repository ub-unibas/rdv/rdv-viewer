/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {Basket} from '../models/basket.model';

/**
 * @ignore
 */
export enum BasketActionTypes {
  AddBasket = '[Basket] Add Basket',
  UpsertBasket = '[Basket] Upsert Basket',
  AddBaskets = '[Basket] Add Baskets',
  UpdateBasket = '[Basket] Update Basket',
  DeleteBasket = '[Basket] Delete Basket',
  ClearBaskets = '[Basket] Clear Baskets',
  SelectBasket = '[Basket] Select Basket',
}

/**
 * Adds one basket
 */
export class AddBasket implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.AddBasket;

  /**
   * Contains new basket
   * @param payload {Object}
   */
  constructor(public payload: { basket: Basket }) {
  }
}

/**
 * Edits content of a basket. If the basket doesn't yet exist, insert as new basket.
 */
export class UpsertBasket implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.UpsertBasket;

  /**
   * Contains edited or new basket
   * @param payload {Object}
   */
  constructor(public payload: { basket: Basket }) {
  }
}

/**
 * Adds several baskets at once
 */
export class AddBaskets implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.AddBaskets;

  /**
   * Contains new baskets
   * @param payload {Object}
   */
  constructor(public payload: { baskets: Basket[] }) {
  }
}

/**
 * Edits an already existing basket
 */
export class UpdateBasket implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.UpdateBasket;

  /**
   * Contains edited basket
   * @param payload {Object}
   */
  constructor(public payload: { basket: Update<Basket> }) {
  }
}

/**
 * Removes a basket
 */
export class DeleteBasket implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.DeleteBasket;

  /**
   * Contains id of basket to be removed
   * @param payload {Object}
   */
  constructor(public payload: { id: string }) {
  }
}

/**
 * Removes all baskets
 */
export class ClearBaskets implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.ClearBaskets;
}

/**
 * Selects a specific basket
 */
export class SelectBasket implements Action {
  /**
   * @ignore
   */
  readonly type = BasketActionTypes.SelectBasket;

  /**
   * Contains id of basket
   * @param payload {string}
   */
  constructor(public payload: { id: string }) {
  }
}


/**
 * @ignore
 */
export type BasketActions =
  AddBasket
  | UpsertBasket
  | AddBaskets
  | UpdateBasket
  | DeleteBasket
  | ClearBaskets
  | SelectBasket;
