/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

import {Action} from '@ngrx/store';

/**
 * @ignore
 */
export enum RemoteFilterConfigsActionTypes {
  GetRemoteFilterFieldOptions = '[UserConfig] Get Remote Filter Field Options',
  SetRemoteFilterFieldOptions = '[UserConfig] Set Remote Filter Field Options',
  OptionRetrieveError = '[UserConfig] Option Retrieve Error',
}


/**
 * Action triggered when remote filter field options are required
 */
export class GetRemoteFilterFieldOptions implements Action {
  /**
   * @ignore
   */
  readonly type = RemoteFilterConfigsActionTypes.GetRemoteFilterFieldOptions;

  /**
   * Contains filter name and URL pointing to remote value
   * @param {Object} payload Object with key and URL
   */
  constructor(public payload: { key: string, url: string }) {
  }
}

/**
 * Action triggered when remote filter field options were received
 */
export class SetRemoteFilterFieldOptions implements Action {
  /**
   * @ignore
   */
  readonly type = RemoteFilterConfigsActionTypes.SetRemoteFilterFieldOptions;

  /**
   * Contains fetched value for filter
   * @param {Object} payload Object with key and fetched value
   */
  constructor(public payload: { key: string, value: string }) {
  }
}

/**
 * Action triggered when remote filter field options request was unsuccessful
 */
export class OptionRetrieveError implements Action {
  /**
   * @ignore
   */
  readonly type = RemoteFilterConfigsActionTypes.OptionRetrieveError;

  /**
   * Contains error
   * @param {Object} payload Error
   */
  constructor(public payload: any) {
  }
}


/**
 * @ignore
 */
export type RemoteFilterConfigsActions
  = GetRemoteFilterFieldOptions
  | SetRemoteFilterFieldOptions
  | OptionRetrieveError;
