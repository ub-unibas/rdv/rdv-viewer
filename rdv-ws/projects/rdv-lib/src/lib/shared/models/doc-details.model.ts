/*
 * Copyright © 2019 Franck Borel, Martin Helfer, Sebastian Schüpbach
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

/*
 * These types are suitable for /object_view and /object_edit responses.
 * "Doc" is the main type.
 */

export interface FacetValue {
  value: any;
  label: string;
}

export interface SelectedFacetValues {
  values: FacetValue[];
  operator: string; // NOT, OR, AND
}

export interface FacetSelection {
  // "key" is a facet's field name
  [key: string]: SelectedFacetValues;
}

export type FieldType = 'str' | 'string_long' | 'int' | 'date' | 'boolean' | 'html';

export interface DocFieldEdit {
  edit?: boolean;
  repeatable: boolean;
  data_type: FieldType;
  show_empty: boolean;
  strict: boolean;
  service?: string;
  required?: boolean;
}

export interface DocFieldValue {
  id?: string;
  label: string;
  // either link or facetSelection is set
  link?: string; // url
  selection?: FacetSelection;
}

export interface LocalizedLabel {
  // key is the language e.g. "de"
  [key: string]: (string | string[]);
}

export interface LocalizedValue {
  // key is the language e.g. "de"
  [key: string]: (number | string | DocFieldValue)[];
}

export interface DocField {
  field_id?: string;
  label: string | LocalizedLabel;
  value: number | string | boolean | LocalizedValue | string[];
  edit_service?: DocFieldEdit;
  group?: string;
}

export interface FuncMetaData {
  preview_image?: string; // url
  title?: string;
  id: string;
  object_type?: string;
  iiif_manifest?: string; // url
  viewer?: string[]; // ignored, env definition is used
  search_after_values?: string;
  groups?: { [key: string]: GroupInfo };
}

export interface GroupInfo {
  order: number;
  open: boolean;
}

export interface Doc {
  desc_metadata: DocField[];
  func_metadata: FuncMetaData;
}

export interface EditDocField {
  field_id: string;
  values: SelectedAutoCompletValue[];
  edit_service?: DocFieldEdit;
}

export interface EditDoc {
  [key: string]: EditDocField;
}

export enum FieldCase {
  'STRING' = 'STRING',
  'NUMBER' = 'NUMBER',
  'OBJ_LABEL' = 'OBJ_LABEL',
  'OBJ_LINK' = 'OBJ_LINK',
  'OBJ_FACET' = 'OBJ_FACET',
  'UNKNOWN' = 'UNKNOWN',
}

export enum ValueCase {
  'DATE' = 'DATE',
  'STRING_SHORT' = 'STRING_SHORT',
  'STRING_HTML' = 'STRING_HTML',
  'SERVICE' = 'SERVICE',
  'STRING_LONG' = 'STRING_LONG',
  'INT' = 'INT',
  'BOOL' = 'BOOL',
  'OTHER' = 'OTHER',
}

export interface BasicAutoCompleteValue {
  // fields from suggestion's endpoint
  label: string;
  service_label?: string; // this field is also used by <ng-select>

  // <ng-select> expects id too (copied from value.id)
  id?: string;

  // eiter a key into env.facetFields or {de: {label: "..."}, en: {label: "..."}}
  group?: string | LocalizedLabel;

  // the following fields are only set if an
  // instance is created from a DocFieldValue (see DetailedComponent.initEditData())
  // either link or facetSelection is set
  link?: string; // url
  selection?: FacetSelection;
}

export interface AutoCompleteValue extends BasicAutoCompleteValue {
  value: { id: string };
}

export interface SelectedAutoCompletValue extends BasicAutoCompleteValue {
  value: { id: string | boolean };
}
