import {TranslateCompiler} from "@ngx-translate/core";
import {environment} from '../Environment';
import {mergeDeep} from '../utils';

export class EnvTranslateCompiler implements TranslateCompiler {

  compile(value: string, lang: string): string | Function {
    return value;
  }

  compileTranslations(translations: any, lang: string): any {
    if (environment.i18n && environment.i18n[lang]) {
      mergeDeep(translations, environment.i18n[lang]);
    }
    return translations;
  }

}
