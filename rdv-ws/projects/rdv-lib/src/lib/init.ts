import {SettingsModel} from "./shared/models/settings.model";
import {initEnv} from "./shared/Environment";
import {initFacetReducer} from "./search/reducers/facet.reducer";
import {initFormReducer} from "./search/reducers/form.reducer";
import {initLayoutReducer} from "./search/reducers/layout.reducer";
import {initQueryReducer} from "./search/reducers/query.reducer";
import {initRemoteFilterConfigsReducer} from "./core/reducers/remote-filter-configs.reducer";
import {initCoreReducers} from "./reducers/index";

export function initRdvLib(environment: SettingsModel): void {
  initEnv(environment);
  initFacetReducer();
  initFormReducer();
  initLayoutReducer();
  initQueryReducer();
  initRemoteFilterConfigsReducer();
  initCoreReducers();
}
